
# Tests

This is an example project of how you can make tests for a simple website about simple things

## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:

```npm install -g yarn```

you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:

```
 yarn
```

### Opening the page in your browser

Once you fork this library to your own GitLab a CI process will start runing and after it has passed you should be able to run the page under settings > pages

### how to contribute to the project

Farðu inn í main.js ..
Í línu 3, 5 og 7 eru 3 variables: name, shoes og color ..
Fyrir aftan '< h1>Nafnið þitt:</ h1>' skrifar þú nafnið þitt inn í < h2>nafnið þitt</ h2> element ..
Fyrir aftan '< span>Þínir skór:</ span>' skrifar þú uppáhalds strigaskóna þína inn í < span id="shoes">Skórnir þínir</ span> element ..
Fyrir aftan '< span>Litur:</ span>' skrifar þú hvernig skórnir eiga að vera á litinn, inn í < span id="color">Skórnir þínir</ span> element ..

## Running the tests

Before you commit your code and do a merge request you should test your code by typing

```yarn test```

into your terminal. If everything is successful you should see something like:

```
Test Suites: 3 passed, 3 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        1.089s
Ran all test suites.
Done in 1.76s.
```
